{
	description = "Wire color swatch generator";

	inputs = {
		nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
	};

	outputs = { self, nixpkgs, ... }:
	let
		system = "x86_64-linux";
		pkgs = nixpkgs.legacyPackages.${system};
		buildPkgs = with pkgs; [
				# graphics libs
				#xorg.libX11
				#xorg.libXcursor
				#xorg.libXrandr
				#xorg.libXi
			];
		libPath = with pkgs; lib.makeLibraryPath buildPkgs;
	in {
		devShells.x86_64-linux.default = pkgs.mkShell {
			nativeBuildInputs = [ pkgs.pkg-config ];
			buildInputs = buildPkgs;
			LD_LIBRARY_PATH = libPath;
		};
	};
}
