#![deny(unsafe_op_in_unsafe_fn)]
use std::alloc::Layout;
use std::borrow::Cow;
use std::ffi::{c_char, c_int, c_long, c_uchar, c_uint, c_ulong, c_void, CStr, CString};
use std::mem::{size_of, MaybeUninit};
use std::process::ExitCode;

use once_cell::{sync::Lazy, sync::OnceCell as Once};
use x11_dl::xlib::{
	self, Atom, ButtonPress, ButtonPressMask, ButtonRelease, ButtonReleaseMask, ClientMessage,
	ClientMessageData, CurrentTime, Display, False, GrabModeAsync, GrabModeSync, GrabSuccess,
	SubstructureNotifyMask, SubstructureRedirectMask, True, Window, XClientMessageEvent, XEvent,
	Xlib, XA_CARDINAL, XA_STRING, XA_WINDOW, XID,
};
use x11_dl::xmu::Xmu;

const MAX_PROPERTY_VALUE_LEN: c_long = 4096;
const SELECT_WINDOW_MAGIC: *const c_char = c":SELECT:".as_ptr();
const ACTIVE_WINDOW_MAGIC: *const c_char = c":ACTIVE:".as_ptr();

// NOTE: Not defined in `x11_dl` but is 0 according to X docs
const NONE: XID = 0;

const NULL: *const u8 = std::ptr::null();
const NULL_MUT: *mut u8 = std::ptr::null_mut();

// TODO: use these in
const FALSE: c_int = 0;
const TRUE: c_int = 1;

#[repr(C)]
struct Options {
	verbose: c_int,
	force_utf8: c_int,
	show_class: c_int,
	show_pid: c_int,
	show_geometry: c_int,
	match_by_id: c_int,
	match_by_cls: c_int,
	full_window_title_match: c_int,
	wa_desktop_titles_invalid_utf8: c_int,
	param_window: &'static CStr,
	param: &'static CStr,
}

static OPTIONS: Once<Options> = Once::new();

static XLIB: Lazy<Xlib> = Lazy::new(|| Xlib::open().unwrap());
static XMU: Lazy<Xmu> = Lazy::new(|| Xmu::open().unwrap());

fn main() -> Result<(), Box<dyn std::error::Error>> {
	Ok(())
}

fn client_msg(
	display: *mut Display,
	window: Window,
	msg: *const c_char,
	data: ClientMessageData,
) -> ExitCode {
	const MASK: c_long = SubstructureRedirectMask | SubstructureNotifyMask;

	let xlib: &Xlib = &XLIB;

	let mut event = XEvent::from(XClientMessageEvent {
		type_: ClientMessage,
		serial: 0,
		send_event: 1,
		display,
		window,
		// Safety: TODO
		// Direct translation from `wmctrl`'s client_msg function
		message_type: unsafe { (xlib.XInternAtom)(display, msg, 0) },
		format: 32,
		data,
	});

	// Safety: TODO
	// Direct translation from `wmctrl`'s client_msg function
	if unsafe {
		(xlib.XSendEvent)(
			display,
			(xlib.XDefaultRootWindow)(display),
			0,
			MASK,
			&mut event as *mut XEvent,
		)
	} != 0
	{
		ExitCode::SUCCESS
	} else {
		// Safety: TODO
		// Assuming msg is a valid c-string
		let msg = unsafe { CStr::from_ptr(msg) }.to_string_lossy();
		eprintln!("Cannot send {msg} event.");
		ExitCode::FAILURE
	}
}

fn wm_info(display: *mut Display) -> ExitCode {
	let xlib: &Xlib = &XLIB;

	// Safety: TODO
	// Direct translation from `wmctrl`'s wm_info function.
	let root_window = unsafe { (xlib.XDefaultRootWindow)(display) };

	todo!();

	ExitCode::SUCCESS
}

/// # Safety
///
/// `display` must be a valid pointer for reading and writing
unsafe fn list_desktops(display: *mut Display) -> ExitCode {
	let xlib: &Xlib = &XLIB;
	// Safety: TODO
	// Direct translation from `wmctrl`'s list_desktops function.
	let root_window = unsafe { (xlib.XDefaultRootWindow)(display) };

	let mut num_desktops = None;
	let mut cur_desktops = None;
	let mut desktop_list_size: c_ulong = 0;
	let mut desktop_geometry = None;
	let mut desktop_geometry_size: c_ulong = 0;
	let desktop_geometry_str: *const *mut c_char = NULL.cast();
	let mut desktop_viewport = None;
	let mut desktop_viewport_size: c_ulong = 0;
	let mut desktop_workarea = None;
	let mut desktop_workarea_size: c_ulong = 0;
	let desktop_workarea_str: *const *mut c_char = NULL.cast();
	let mut list = None;
	let mut i: c_int = 0;
	let mut id: c_int = 0;
	let mut ret = ExitCode::FAILURE;
	let names: *mut *mut c_char = NULL_MUT.cast();
	let names_layout = Layout::new::<()>();
	let mut names_are_utf8: c_int = TRUE;

	// NOTE:
	// This loop will not be executed more than once and is used ONLY as a way to skip to the end
	// of the function in order to ensure all values are appropriately cleaned up before the
	// function exits and not need to repeat a bunch of cleanup code everywhere an early return
	// would otherwise occur
	loop {
		// Safety: TODO
		// Direct translation from `wmctrl`'s list_destkops function.
		num_desktops = unsafe {
			get_property(
				display,
				root_window,
				XA_CARDINAL,
				c"_NET_NUMBER_OF_DESKTOPS".as_ptr(),
				NULL_MUT.cast(),
			)
		}
		.map(|(ptr, layout)| (ptr.cast::<c_ulong>(), layout));
		if num_desktops.is_none() {
			// Safety: TODO
			// Direct translation from `wmctrl`'s list_destkops function.
			num_desktops = unsafe {
				get_property(
					display,
					root_window,
					XA_CARDINAL,
					c"_WIN_WORKSPACE_COUNT".as_ptr(),
					NULL_MUT.cast(),
				)
			}
			.map(|(ptr, layout)| (ptr.cast::<c_ulong>(), layout));
			if num_desktops.is_none() {
				eprintln!("Cannot get number of desktops properties. (_NET_NUMBER_OF_DESKTOPS or _WIN_WORKSPACE_COUNT)");
				break;
			}
		}

		// Safety: TODO
		// Direct translation from `wmctrl`'s list_destkops function.
		cur_desktops = unsafe {
			get_property(
				display,
				root_window,
				XA_CARDINAL,
				c"_NET_CURRENT_DESKTOP".as_ptr(),
				NULL_MUT.cast(),
			)
		}
		.map(|(ptr, layout)| (ptr.cast::<c_ulong>(), layout));
		if cur_desktops.is_none() {
			// Safety: TODO
			// Direct translation from `wmctrl`'s list_destkops function.
			cur_desktops = unsafe {
				get_property(
					display,
					root_window,
					XA_CARDINAL,
					c"_WIN_WORKSPACE".as_ptr(),
					NULL_MUT.cast(),
				)
			}
			.map(|(ptr, layout)| (ptr.cast::<c_ulong>(), layout));
			if cur_desktops.is_none() {
				eprintln!("Cannot get current desktop properties. (_NET_CURRENT_DESKTOP or _WIN_WORKSPACE property)");
				break;
			}
		}

		// Safety: TODO
		// Direct translation from `wmctrl`'s list_destkops function.
		list = unsafe {
			get_property(
				display,
				root_window,
				(xlib.XInternAtom)(display, c"UTF8_STRING".as_ptr(), False),
				c"_NET_DESKTOP_NAMES".as_ptr(),
				&mut desktop_list_size,
			)
		};
		// The options is initialized in main() so waiting shouldn't block, this code is run
		// synchronisly as well so there isn't a chance of a deadlock.
		let options = OPTIONS.wait();
		if options.wa_desktop_titles_invalid_utf8 != FALSE || list.is_none() {
			names_are_utf8 = FALSE;
			// Safety: TODO
			// Direct translation from `wmctrl`'s list_destkops function.
			list = unsafe {
				get_property(
					display,
					root_window,
					XA_STRING,
					c"_WIN_WORKSPACE_NAMES".as_ptr(),
					&mut desktop_list_size,
				)
			};
			if list.is_none() && options.verbose != FALSE {
				eprintln!("Cannot get desktop names properties. (_NET_DESKTOP_NAMES _WIN_WORKSPACE_NAMES)");
				// ignore the error - list the desktops without names
			}
		}

		// Safety: TODO
		// Direct translation from `wmctrl`'s list_destkops function.
		desktop_geometry = unsafe {
			get_property(
				display,
				root_window,
				XA_CARDINAL,
				c"_NET_DESKTOP_GEOMETRY".as_ptr(),
				&mut desktop_geometry_size,
			)
		}
		.map(|(ptr, layout)| (ptr.cast::<c_ulong>(), layout));
		if desktop_geometry.is_none() && options.verbose != FALSE {
			eprintln!("Cannot get common size of all desktop (_NET_DESKTOP_GEOMETRY).");
		}

		// Safety: TODO
		// Direct translation from `wmctrl`'s list_destkops function.
		desktop_viewport = unsafe {
			get_property(
				display,
				root_window,
				XA_CARDINAL,
				c"_NET_DESKTOP_VIEWPORT".as_ptr(),
				&mut desktop_viewport_size,
			)
		}
		.map(|(ptr, layout)| (ptr.cast::<c_ulong>(), layout));
		if desktop_viewport.is_none() && options.verbose != FALSE {
			eprintln!("Cannot get common size of all desktops (_NET_DESKTOP_VIEWPORT).");
		}

		// Safety: TODO
		// Direct translation from `wmctrl`'s list_destkops function.
		desktop_workarea = unsafe {
			get_property(
				display,
				root_window,
				XA_CARDINAL,
				c"_NEW_WORKAREA".as_ptr(),
				&mut desktop_viewport_size,
			)
		}
		.map(|(ptr, layout)| (ptr.cast::<c_ulong>(), layout));
		if desktop_workarea.is_none() {
			// Safety: TODO
			// Direct translation from `wmctrl`'s list_destkops function.
			desktop_workarea = unsafe {
				get_property(
					display,
					root_window,
					XA_CARDINAL,
					c"_WIN_WORKAREA".as_ptr(),
					&mut desktop_viewport_size,
				)
			}
			.map(|(ptr, layout)| (ptr.cast::<c_ulong>(), layout));
			if desktop_workarea.is_none() && options.verbose != FALSE {
				eprintln!("Cannot get _NET_WORKAREA property.");
			}
		}

		if let Some((num_desktops_ptr, num_desktops_layout)) = num_desktops {
			// Safety: TODO
			let num_desktops_deref = unsafe { *num_desktops_ptr };
			names_layout = Layout::array::<*const c_char>(num_desktops_deref as usize).unwrap();
			names = unsafe { std::alloc::alloc(names_layout).cast() };
			if let Some((list_ptr, list_layout)) = list {
				id = 0;
				// Safety: TODO
				let names_offset = unsafe { names.offset(id as isize) };
				// Safety: TODO
				unsafe { names_offset.write(list_ptr) };
				id += 1;
				'l: for i in 0..desktop_list_size {
					let list_offset = list_ptr.offset(i as isize);
					let list_offset_deref = *list_offset;
					if list_offset_deref == b'\0' as c_char {
						if id as c_ulong >= num_desktops_deref {
							break 'l;
						}

						// Safety: TODO
						let names_offset = unsafe { names.offset(id as isize) };
						// Safety: TODO
						let list_offset = unsafe { list_ptr.offset(i as isize + 1) };
						// Safety: TODO
						unsafe { names_offset.write(list_offset) };
						id += 1;
					}
				}
			}
		}

		// TODO: Continue from line 1113 in main.c

		break;
	}

	std::alloc::dealloc(names as *mut u8, names_layout);
	if let Some((ptr, layout)) = num_desktops {
		std::alloc::dealloc(ptr as *mut u8, layout);
	}
	if let Some((ptr, layout)) = cur_desktops {
		std::alloc::dealloc(ptr as *mut u8, layout);
	}
	if let Some((ptr, layout)) = desktop_geometry {
		std::alloc::dealloc(ptr as *mut u8, layout);
	}
	// TODO: free desktop_geometry_str
	if let Some((ptr, layout)) = desktop_viewport {
		std::alloc::dealloc(ptr as *mut u8, layout);
	}
	// TODO: free desktop_viewport_str
	if let Some((ptr, layout)) = desktop_workarea {
		std::alloc::dealloc(ptr as *mut u8, layout);
	}
	// TODO: free desktop_workarea_str
	if let Some((ptr, layout)) = list {
		std::alloc::dealloc(ptr as *mut u8, layout);
	}

	ret
}

/// # Safety
///
/// `strv` must be a valid pointer to an array of pointers to valid nul-terminated c-strings.
unsafe fn longest_str(strv: *const *mut c_char) -> c_int {
	let mut max: c_int = 0;
	let mut i: c_int = 0;

	if strv.is_null() {
		return 0;
	}

	loop {
		// Safety: TODO
		// Direct translation from `wmctrl`'s longest_str function.
		let str = unsafe { strv.offset(i as isize) };
		if str.is_null() {
			break;
		}

		// Safety:
		// User promises the provided array of pointers are valid c-style strings
		let str = unsafe { CStr::from_ptr(*str) };
		let str_count = str.count_bytes() as c_int;
		if str_count > max {
			max = str_count;
		}
		i += 1;
	}

	max
}

/// # Safety
///
/// Same requirements as [`get_property`]
unsafe fn get_client_list(
	display: *mut Display,
	size: *mut c_ulong,
) -> Option<(*mut Window, Layout)> {
	let xlib: &Xlib = &XLIB;
	// Safety: TODO
	// Direct transloation from `wmctrl`'s get_client_list function
	let root_window = unsafe { (xlib.XDefaultRootWindow)(display) };
	// Safety: TODO
	// Direct transloation from `wmctrl`'s get_client_list function
	let mut client_list = unsafe {
		get_property(
			display,
			root_window,
			XA_WINDOW,
			c"_WIN_CLIENT_LIST".as_ptr(),
			size,
		)
	};
	if client_list.is_none() {
		client_list = unsafe {
			get_property(
				display,
				root_window,
				XA_CARDINAL,
				c"_WIN_CLIENT_LIST".as_ptr(),
				size,
			)
		};
	}
	if client_list.is_none() {
		eprintln!("Cannot get client list properties.\n(_NET_CLIENT_LIST or _WIN_CLIENT_LIST)");
	};

	client_list.map(|(ptr, layout)| (ptr.cast::<Window>(), layout))
}

fn list_windows(display: *mut Display) -> ExitCode {
	let mut max_client_machine_len: c_int = 0;
	// size of the `client_list` memory region in bytes
	let mut client_list_size: c_ulong = 0;

	// Safety: TODO
	// Direct tranlsation of `wmctrl`'s list_windows function
	let Some(client_list) = (unsafe { get_client_list(display, &mut client_list_size) }) else {
		return ExitCode::FAILURE;
	};
	// Safety: TODO this relies on the X library returning correct pointers
	let client_list_slice = unsafe {
		std::slice::from_raw_parts(
			client_list.0 as *mut Window,
			client_list_size as usize / size_of::<Window>(),
		)
	};

	let xlib: &Xlib = &XLIB;
	// Safety: TODO
	// Direct translation from `wmctrl`'s list_windows function
	let root_window = unsafe { (xlib.XDefaultRootWindow)(display) };

	for client in client_list_slice.iter() {
		let client_machine = unsafe {
			get_property(
				display,
				*client,
				XA_STRING,
				c"WM_CLIENT_MACHINE".as_ptr(),
				NULL_MUT,
			)
		};

		if let Some((ptr, layout)) = client_machine {
			// Safety: TODO
			// Direct translation from `wmctrl`'s list_windows function.
			// NOTE: `CStr::count_bytes` will be stabilized in 1.79, so lets just wait until then
			// and live with this error until its stabilized.
			max_client_machine_len = unsafe { CStr::from_ptr(ptr) }.count_bytes() as c_int;
			// Safety:
			// `ptr` was alloc'd from `std::alloc::alloc` with `layout`.
			unsafe { std::alloc::dealloc(ptr as *mut u8, layout) };
		}

		// Safety: TODO
		// Direct translation from `wmctrl`'s list_windows funtion.
		let title_utf8 = unsafe { get_window_title(display, *client) };
		// Safety: TODO
		// Direct translation from `wmctrl`'s list_windows funtion.
		let title_out = unsafe { get_output_str(title_utf8, TRUE) };
		// Safety: TODO
		// Direct translation from `wmctrl`'s list_windows funtion.
		let class_out = unsafe { get_window_class(display, *client) };

		// Safety: TODO
		// Direct translation from `wmctrl`'s list_windows funtion.
		let mut desktop = unsafe {
			get_property(
				display,
				*client,
				XA_CARDINAL,
				c"_NET_WM_DESKTOP".as_ptr(),
				NULL_MUT.cast(),
			)
		};
		if desktop.is_none() {
			// Safety: TODO
			// Direct translation from `wmctrl`'s list_windows funtion.
			desktop = unsafe {
				get_property(
					display,
					*client,
					XA_CARDINAL,
					c"_WIN_WORKSPACE".as_ptr(),
					NULL_MUT.cast(),
				)
			};
		}

		// Safety: TODO
		// Direct translation from `wmctrl`'s list_windows funtion.
		let client_machine = unsafe {
			get_property(
				display,
				*client,
				XA_STRING,
				c"WM_CLIENT_MACHINE".as_ptr(),
				NULL_MUT.cast(),
			)
		};

		// Safety: TODO
		// Direct translation from `wmctrl`'s list_windows funtion.
		let Some(pid) = (unsafe {
			get_property(
				display,
				*client,
				XA_CARDINAL,
				c"_NET_WM_PID".as_ptr(),
				NULL_MUT.cast(),
			)
		}) else {
			return ExitCode::FAILURE;
		};

		let mut x: c_int = 0;
		let mut y: c_int = 0;
		let mut junkx: c_int = 0;
		let mut junky: c_int = 0;
		let mut wwidth: c_uint = 0;
		let mut wheight: c_uint = 0;
		let mut bw: c_uint = 0;
		let mut depth: c_uint = 0;
		let mut junkroot: Window = 0;
		// Safety: TODO
		// Direct translation from `wmctrl`'s list_windows function.
		unsafe {
			(xlib.XGetGeometry)(
				display,
				*client,
				&mut junkroot,
				&mut junkx,
				&mut junky,
				&mut wwidth,
				&mut wheight,
				&mut bw,
				&mut depth,
			)
		};
		unsafe {
			(xlib.XTranslateCoordinates)(
				display,
				*client,
				junkroot,
				junkx,
				junky,
				&mut x,
				&mut y,
				&mut junkroot,
			)
		};

		// FIXME vvvvvvvv: Figure out the formatting on these print statements
		print!("0x{:.8x} {:2}", &client, desktop.as_ptr() as usize);
		if OPTIONS.show_pid {
			print!(" {:6}", pid.0 as usize);
		}
		if OPTIONS.show_geometry {
			print!(" {:4} {:4} {:4} {:4}", x, y, wwidth, wheight);
		}
		if OPTIONS.show_class {
			print!(
				" {:20}",
				if class_out.is_empty() {
					"N/A"
				} else {
					&class_out
				}
			);
		}
		// FIXME: This is the ugliest thing I've ever written in rust and I hate it
		println!(
			" {} {}",
			if let Some((ptr, layout)) = client_machine {
				// Safety: TODO
				let str = unsafe { CStr::from_ptr(ptr) };
				if str.count_bytes() > max_client_machine_len as usize {
					let mut slice = str.to_bytes();
					slice[max_client_machine_len as usize] = 0;
					CStr::from_bytes_with_nul(slice).unwrap().to_string_lossy()
				} else {
					str.to_string_lossy()
				}
			} else {
				Cow::from("N/A")
			},
			if let Some((ptr, layout)) = title_out {
				// Safety: TODO
				unsafe { CStr::from_ptr(ptr) }.to_string_lossy()
			} else {
				Cow::from("N/A")
			}
		);
		// FIXME ^^^^^^^^: Figure out the formatting on these print statements

		if let Some((ptr, layout)) = title_out {
			// Safety:
			// `ptr` was alloc'd from `std::alloc::alloc` with `layout`.
			unsafe { std::alloc::dealloc(ptr as *mut u8, layout) };
		}
		if let Some((ptr, layout)) = desktop {
			// Safety:
			// `ptr` was alloc'd from `std::alloc::alloc` with `layout`.
			unsafe { std::alloc::dealloc(ptr as *mut u8, layout) };
		}
		if let Some((ptr, layout)) = client_machine {
			// Safety:
			// `ptr` was alloc'd from `std::alloc::alloc` with `layout`.
			unsafe { std::alloc::dealloc(ptr as *mut u8, layout) };
		}
		// Safety:
		// `ptr` was alloc'd from `std::alloc::alloc` with `layout`.
		unsafe { std::alloc::dealloc(pid.0 as *mut u8, pid.1) };
	}

	// Safety:
	// `ptr` was alloc'd from `std::alloc::alloc` with `layout`.
	unsafe { std::alloc::dealloc(client_list.0 as *mut u8, client_list.1) };

	ExitCode::SUCCESS
}

/// # Safety
///
/// - `display` must be a valid writable(FIXME: Does it need to be writable) pointer
unsafe fn get_window_class(display: *mut Display, window: Window) -> String {
	let mut size: c_ulong = 0;
	// Safety:
	// Direct translation from `wmctrl`'s get_window_class function.
	let wm_class =
		unsafe { get_property(display, window, XA_STRING, c"WM_CLASS".as_ptr(), &mut size) };

	let class_utf8 = if let Some((ptr, layout)) = wm_class {
		let mut found = None;
		for i in 0..layout.size() {
			// Safety:
			// The resulting pointer will be inbounds and won't overflow
			let offset = unsafe { ptr.offset(i as isize) };
			// FIXME: Should this be a loop? If there's one null before the end of the property bytes, why couldn't there be more than one?
			if unsafe { *offset } == b'\0' as c_char {
				found = Some(i);
				break;
			}
		}
		if let Some(found) = found {
			// Safety:
			// The resulting pointer will be inbounds and won't overflow
			let offset = unsafe { ptr.offset(found as isize) };
			// Safety:
			// The pointer is valid for writes, and is aligned as it is a pointer to single bytes.
			unsafe { offset.write(b'.' as c_char) };
		}

		// Safety:
		// The property's constructor ensures its pointer is valid or null.
		let string = unsafe { ensure_utf8(ptr) };
		// Safety:
		// `ptr` was alloc'd from `std::alloc::alloc` with `layout`.
		unsafe { std::alloc::dealloc(ptr as *mut u8, layout) };
		string
	} else {
		String::new()
	};

	class_utf8
}

/// # Safety
///
/// - `cstr` must be a valid readable pointer to a nul-terminated c-string or null.
unsafe fn ensure_utf8(cstr: *const c_char) -> String {
	debug_assert!(
		!cstr.is_null(),
		"Provided pointer to `ensure_utf8` was null!"
	);
	// Safety:
	// Caller promises the pointer is a valid c-string.
	unsafe { CStr::from_ptr(cstr) }.to_str().unwrap().to_owned()
}

/// # Safety
///
/// - `display` must be a valid writable(FIXME: Does it need to be writable) pointer
unsafe fn get_window_title(display: *mut Display, window: Window) -> String {
	let xlib: &Xlib = &XLIB;

	// Safety: TODO
	// Direct translation from `wmctrl`'s get_window_title function.
	let wm_name = unsafe {
		get_property(
			display,
			window,
			XA_STRING,
			c"WM_NAME".as_ptr(),
			NULL_MUT.cast(),
		)
	};

	// Safety: TODO
	// Direct translation from `wmctrl`'s get_window_title function.
	let utf8_string = unsafe { (xlib.XInternAtom)(display, c"UTF8_STRING".as_ptr(), False) };
	// Safety: TODO
	// Direct translation from `wmctrl`'s get_window_title function.
	let net_wm_name = unsafe {
		get_property(
			display,
			window,
			utf8_string,
			c"_NET_WM_NAME".as_ptr(),
			NULL_MUT.cast(),
		)
	};

	let title_utf8 = if let Some((ptr, layout)) = net_wm_name {
		// Safety: TODO
		let string = unsafe { ensure_utf8(ptr) };
		// Safety:
		// `ptr` was alloc'd from `std::alloc::alloc` with `layout`.
		unsafe { std::alloc::dealloc(ptr as *mut u8, layout) };
		string
	} else if let Some((ptr, layout)) = wm_name {
		// Safety: TODO
		let string = unsafe { ensure_utf8(ptr) };
		// Safety:
		// `ptr` was alloc'd from `std::alloc::alloc` with `layout`.
		unsafe { std::alloc::dealloc(ptr as *mut u8, layout) };
		string
	} else {
		String::new()
	};

	title_utf8
}

// use property::PropertyPtr;
// mod property {
// 	use std::{
// 		alloc::Layout,
// 		ffi::{c_char, c_uchar},
// 		ptr::NonNull,
// 	};
// 	pub(crate) struct PropertyPtr {
// 		ptr: *mut c_char,
// 		layout: std::alloc::Layout,
// 	}
// 	impl PropertyPtr {
// 		/// # Safety
// 		///
// 		/// `src` must be a valid pointer containing at least `size` bytes of valid memory to read
// 		/// from.
// 		pub unsafe fn new(src: *mut c_uchar, size: u64) -> Self {
// 			assert_ne!(
// 				src,
// 				NULL_MUT,
// 				"Can't copy data from a null pointer!"
// 			);
// 			let layout = std::alloc::Layout::array::<c_char>(size as usize + 1).unwrap();
// 			// Safety:
// 			// `alloc()` requires the layout to have a non-zero size, and we ensure the layout's
// 			// size is at minimum 1 byte, which additionally is needed to ensure we have a valid
// 			// nul-terminated c-string.
// 			let ptr = unsafe { std::alloc::alloc(layout) } as *mut c_char;
// 			// Safety:
// 			// `src` is assumed init by the call to `XGetWindowProperty` (FIXME verify the
// 			// previous), `ptr` is freshly alloc'd from the global allocator so is valid and aligned, and
// 			// the pointers are not expected to overlap.
// 			unsafe { std::ptr::copy_nonoverlapping(src, ptr as *mut u8, size as usize) };
// 			// Safety:
// 			// `ptr` was allocated with a size of `size + 1` so offsetting by `size` will
// 			// yield a point in bounds and non-overflowed.
// 			let offset = unsafe { ptr.offset(size as isize) };
// 			// Safety:
// 			// `offset` is the last byte of a c-string, and is guaranteed to be aligned as the data
// 			// type is single bytes.
// 			unsafe { offset.write(0) };
//
// 			Self { ptr, layout }
// 		}
//
// 		pub fn null() -> Self {
// 			Self {
// 				ptr: NULL_MUT.cast(),
// 				layout: std::alloc::Layout::new::<()>(),
// 			}
// 		}
//
// 		/// # Safety
// 		///
// 		/// The caller must ensure that the property this object was constructed from contains the type
// 		/// of data that is attempted to be accessed.
// 		pub unsafe fn read_as<T>(&self) -> &T {
// 			// Safety:
// 			// The user promises that `self.ptr` is valid for reads of type `T`.
// 			unsafe { &*self.ptr.cast::<T>() }
// 		}
//
// 		pub fn as_ptr(&self) -> *mut c_char {
// 			self.ptr
// 		}
// 		pub fn as_slice(&self) -> &[c_char] {
// 			if self.ptr.is_null() {
// 				// Safety:
// 				// As the docs state, creating an empty slice still requires a valid pointer, and
// 				// suggests `NonNull::dangling()` to be used.
// 				unsafe { std::slice::from_raw_parts(NonNull::dangling().as_ptr(), 0) }
// 			} else {
// 				// Safety:
// 				// `self.ptr` was alloc'd with the layout of an array of `c_char`s, so it is valid
// 				// for creating a slice for the size of the allocation.
// 				unsafe { std::slice::from_raw_parts(self.ptr, self.layout.size()) }
// 			}
// 		}
// 		pub fn as_slice_mut(&mut self) -> &mut [c_char] {
// 			if self.ptr.is_null() {
// 				// Safety:
// 				// As the docs state, creating an empty slice still requires a valid pointer, and
// 				// suggests `NonNull::dangling()` to be used.
// 				unsafe { std::slice::from_raw_parts_mut(NonNull::dangling().as_ptr(), 0) }
// 			} else {
// 				// Safety:
// 				// `self.ptr` was alloc'd with the layout of an array of `c_char`s, so it is valid
// 				// for creating a slice for the size of the allocation.
// 				unsafe { std::slice::from_raw_parts_mut(self.ptr, self.layout.size()) }
// 			}
// 		}
//
// 		pub fn is_null(&self) -> bool {
// 			self.ptr.is_null()
// 		}
// 	}
// 	impl Drop for PropertyPtr {
// 		fn drop(&mut self) {
// 			if !self.ptr.is_null() {
// 				// Safety:
// 				// The type's constructor ensure that if `self.ptr` is non-null, it was alloc'd from
// 				// the rust allocator with the layout `self.layout`.
// 				unsafe { std::alloc::dealloc(self.ptr as *mut u8, self.layout) };
// 			}
// 		}
// 	}
// }
/// # Safety
///
/// - `display` must be a valid writable pointer pointing to a [`Display`] object obtained from
///    the X server.
/// - `prop_name` must be a valid nul-terminated c-string.
/// - `size` must be a valid writable pointer.
/// - The pointer returned from this funtion is allocated from [`std::alloc::alloc`] and thus must
/// be freed using [`std::alloc::dealloc`].
/// - The returned value, if [`Some`], is a tuple of a pointer to the property data, guaranteed to
/// be non-null, and the layout with which the pointer was allocated. The pointer is allocated
/// from [`std::alloc::alloc`] and must be freed using [`std::alloc::dealloc`] with the assciated
/// layout.
unsafe fn get_property(
	display: *mut Display,
	window: Window,
	xa_prop_type: Atom,
	prop_name: *const c_char,
	size: *mut c_ulong,
) -> Option<(*mut c_char, Layout)> {
	let xlib: &Xlib = &XLIB;

	// Safety: TODO
	// Direct translation from `wmctrl`'s get_roperty function
	let xa_prop_name = unsafe { (xlib.XInternAtom)(display, prop_name, False) };
	let mut xa_ret_type: Atom = 0; //MaybeUninit::uninit().assume_init();
	let mut ret_format: c_int = 0;
	let mut ret_nitems: c_ulong = 0;
	let mut ret_bytes_after: c_ulong = 0;
	let mut ret_prop: *mut c_uchar = NULL_MUT;

	let get_property_success = unsafe {
		(xlib.XGetWindowProperty)(
			display,
			window,
			xa_prop_name,
			0,
			MAX_PROPERTY_VALUE_LEN / 4,
			False,
			xa_prop_type,
			&mut xa_ret_type,
			&mut ret_format,
			&mut ret_nitems,
			&mut ret_bytes_after,
			&mut ret_prop,
		)
	};

	if get_property_success != xlib::Success.into() {
		if OPTIONS.verbose {
			// Safety: TODO
			// Assuming msg is a valid c-string
			let prop_name = unsafe { CStr::from_ptr(prop_name) }.to_string_lossy();
			eprintln!("Cannot get {prop_name} property.");
		}
		return None;
	}

	if xa_ret_type != xa_prop_type {
		if OPTIONS.verbose {
			// Safety: TODO
			// Assuming msg is a valid c-string
			let prop_name = unsafe { CStr::from_ptr(prop_name) }.to_string_lossy();
			eprintln!("Invalid type of {prop_name} property.");
		}
		// Safety: TODO
		// Direct translation from `wmctrl`'s get_roperty function
		unsafe { (xlib.XFree)(ret_prop as *mut c_void) };
		return None;
	}

	let alloc_size = (ret_format as c_ulong / 8) * ret_nitems;
	let layout = std::alloc::Layout::array::<c_char>(alloc_size as usize + 1).unwrap();
	// Safety:
	// `alloc()` requires the layout to have a non-zero size, and we ensure the layout's
	// size is at minimum 1 byte, which additionally is needed to ensure we have a valid
	// nul-terminated c-string.
	let ptr = unsafe { std::alloc::alloc(layout) } as *mut c_char;
	// Safety:
	// `src` is assumed init by the call to `XGetWindowProperty` (FIXME verify the
	// previous), `ptr` is freshly alloc'd from the global allocator so is valid and aligned, and
	// the pointers are not expected to overlap.
	unsafe { std::ptr::copy_nonoverlapping(ret_prop, ptr as *mut u8, alloc_size as usize) };
	// Safety:
	// `ptr` was allocated with a size of `size + 1` so offsetting by `size` will
	// yield a point in bounds and non-overflowed.
	let offset = unsafe { ptr.offset(alloc_size as isize) };
	// Safety:
	// `offset` is the last byte of a c-string, and is guaranteed to be aligned as the data
	// type is single bytes.
	unsafe { offset.write(0) };
	// Safety: TODO
	// Assuming that if `XGetWindowProperty()` does not error then `ret_prop` must also be valid
	// for the specified format.
	let property = Some((ptr, layout));

	if !size.is_null() {
		// Safety:
		// The caller promises that `size` is valid for writing.
		unsafe { size.write(alloc_size) };
	}

	// Safety: TODO
	// Direct translation from `wmctrl`'s get_property function
	unsafe { (xlib.XFree)(ret_prop as *mut c_void) };

	property
}

fn select_window(display: *mut Display) -> Window {
	let xlib: &Xlib = &XLIB;
	let xmu: &Xmu = &XMU;

	// Safety: TODO
	// Direct transloation from `wmctrl`'s Select_Window function
	let root_window = unsafe { (xlib.XDefaultRootWindow)(display) };
	let mut buttons = 0;

	// NOTE: `XC_crosshair` evidently isn't defined in the `xlib` crate, so I found a list of defines
	// and the matching font here:
	// https://tronche.com/gui/x/xlib/appendix/b/
	// archived: https://web.archive.org/web/20240516125313/https://tronche.com/gui/x/xlib/appendix/b/
	// Maybe in a complete rewrite in the future I'd use another one, like XC_X_cursor.
	// Safety: TODO
	// Direct translation from `wmctrl`'s Select_Window function
	let cursor = unsafe {
		(xlib.XCreateFontCursor)(display, /*XC_crosshair*/ 34)
	};

	// Safety: TODO
	// Direct translation from `wmctrl`'s Select_Window function
	let status = unsafe {
		(xlib.XGrabPointer)(
			display,
			root_window,
			0,
			(ButtonPressMask | ButtonReleaseMask) as u32,
			GrabModeSync,
			GrabModeAsync,
			root_window,
			cursor,
			CurrentTime,
		)
	};
	if status != GrabSuccess {
		eprintln!("ERROR: Cannot grab mouse.");
		// nightly-only API: ExitCode::FAILURE.exit_process();
		std::process::exit(1); // EXIT_FAILURE is defined as 1
	}

	let mut target_win = NONE;
	// FIXME: Is this the best way to initialize an `XEvent`?
	let mut event: XEvent = XEvent { type_: 0 };
	use x11_dl::xlib::{CurrentTime, SyncPointer};
	while target_win == NONE || buttons != 0 {
		// Safety: TODO
		// Direct translation from `wmctrl`'s Select_Window function
		unsafe { (xlib.XAllowEvents)(display, SyncPointer, CurrentTime) };
		unsafe {
			(xlib.XWindowEvent)(
				display,
				root_window,
				ButtonPressMask | ButtonReleaseMask,
				&mut event,
			)
		};
		match event.get_type() {
			ButtonPress => {
				if target_win == NONE {
					// Safety:
					// We told X to notify us of button events, so we should be ok looking at the
					// button field
					target_win = unsafe { event.button }.subwindow;
					if target_win == NONE {
						target_win == root_window;
					}
				}
				buttons += 1;
			},
			ButtonRelease => {
				if buttons > 0 {
					buttons -= 1;
				}
			},
			_ => {
				// NOTE: Received an event we didn't ask for!
			},
		}
	}

	// Safety: TODO
	// Direct translation from `wmctrl`'s Select_Window function
	unsafe { (xlib.XUngrabPointer)(display, CurrentTime) };

	let mut discard1: c_int = 0;
	let mut discard2: c_int = 0;
	let mut discard3: c_uint = 0;
	let mut discard4: c_uint = 0;
	let mut discard5: c_uint = 0;
	let mut discard6: c_uint = 0;
	let mut root_window = root_window;
	// Safety: TODO
	// Direct translation from `wmctrl`'s Select_Window function
	let geometry = unsafe {
		(xlib.XGetGeometry)(
			display,
			target_win,
			&mut root_window,
			&mut discard1,
			&mut discard2,
			&mut discard3,
			&mut discard4,
			&mut discard5,
			&mut discard6,
		)
	};

	if geometry != 0 && target_win != root_window {
		// Safety: TODO
		// Direct translation from `wmctrl`'s Select_Window function
		target_win = unsafe { (xmu.XmuClientWindow)(display, target_win) };
	}

	target_win
}

fn get_active_window(xlib: &Xlib, display: &mut Display) -> Window {
	// Safety: TODO
	// Direct transloation from `wmctrl`'s Select_Window function
	let root_window = unsafe { (xlib.XDefaultRootWindow)(display) };
	let mut size: c_ulong = 0;

	// Safety:
	// - `display` is passed in as a rust mutable reference, which means it is valid writable
	// pointer.
	// - rust's string prefix `c` ensures that the resulting string is a c-style nul-terminated.
	// - `size` is alloc'd on the stack and initialized, and is valid to read/write to.
	// - The returned pointer is dealloc'd below before the function exits.
	let property = unsafe {
		get_property(
			display,
			root_window,
			XA_WINDOW,
			c"_NET_ACTIVE_WINDOW".as_ptr(),
			&mut size,
		)
	};

	if let Some((ptr, layout)) = property {
		// Safety: TODO
		// Direct transloation from `wmctrl`'s Select_Window function
		let window = unsafe { *ptr.cast::<Window>() };
		// Safety:
		// `ptr` was alloc'd from `std::alloc::alloc` with `layout`.
		unsafe { std::alloc::dealloc(ptr as *mut u8, layout) };
		window
	} else {
		NONE
	}
}

#[cfg(test)]
#[test]
fn verify_cli() {
	use clap::CommandFactory;
	Options::command().debug_assert()
}
